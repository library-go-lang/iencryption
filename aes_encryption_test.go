package iencryption_test

import (
	"gitlab.com/library-go-lang/iencryption"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAESEncryptSuccess(t *testing.T) {
	enc := iencryption.NewAESEncryption("0123456789abcdef")

	chiper, err := enc.Encrypt([]byte("password-tes"))

	assert.NoError(t, err, "[TestAESEncryptSuccess] Should not error")
	assert.Less(t, 0, len(chiper), "[TestAESEncryptSuccess] byte should not empty")
}

func TestAESEnccryptErrorInvalidKey(t *testing.T) {
	enc := iencryption.NewAESEncryption("test-key")

	_, err := enc.Encrypt([]byte("cGFzc3dvcmQtdGVz"))

	assert.Error(t, err, "[TestAESDecryptFailed] Should error")
	assert.Equal(t, "crypto/aes: invalid key size 8", err.Error(), "[TestAESDecryptFailed] Error should \"crypto/aes: invalid key size 8\"")
}

func TestAESDecryptSuccess(t *testing.T) {
	enc := iencryption.NewAESEncryption("0123456789abcdef")

	plain, err := enc.Decrypt([]byte("26950ee96d6df80af6d8e2ce87fcb58ace3f1e8234eb360deec3f04b18b8c26ed90b7e99eaa843b2"))

	assert.NoError(t, err, "[TestAESDecryptSuccess] Should not error")
	assert.Less(t, 0, len(plain), "[TestAESDecryptSuccess] byte should not empty")
	assert.Equal(
		t,
		[]byte{112, 97, 115, 115, 119, 111, 114, 100, 45, 116, 101, 115},
		plain,
		"[TestAESDecryptSuccess] Plain should same",
	)
}

func TestAESDecryptErrorInvalidKey(t *testing.T) {
	enc := iencryption.NewAESEncryption("test-key")

	_, err := enc.Decrypt([]byte("cGFzc3dvcmQtdGVz"))

	assert.Error(t, err, "[TestAESDecryptFailed] Should error")
	assert.Equal(t, "crypto/aes: invalid key size 8", err.Error(), "[TestAESDecryptFailed] Error should \"crypto/aes: invalid key size 8\"")
}

func TestAESDecryptErrorMissingSize(t *testing.T) {
	enc := iencryption.NewAESEncryption("0123456789abcdef")

	_, err := enc.Decrypt([]byte("x"))

	assert.Error(t, err, "[TestAESDecryptFailed] Should error")
	assert.Equal(t, "missing size", err.Error(), "[TestAESDecryptFailed] Error should \"missing size\"")
}

func TestAESEncryptStringSuccess(t *testing.T) {
	var enc iencryption.Encryptor = iencryption.NewAESEncryption("0123456789abcdef")

	plain, err := enc.Encrypt([]byte("26950ee96d6df80af6d8e2ce87fcb58ace3f1e8234eb360deec3f04b18b8c26ed90b7e99eaa843b2"))

	s := enc.EncryptString(plain)

	assert.NoError(t, err, "[TestAESEncryptStringSuccess] Should not error")
	assert.NotEmpty(t, s, "[TestAESEncryptStringSuccess] Should not empty")
}

func TestAESDecryptStringSuccess(t *testing.T) {
	var enc iencryption.Decryptor = iencryption.NewAESEncryption("0123456789abcdef")

	plain, err := enc.Decrypt([]byte("26950ee96d6df80af6d8e2ce87fcb58ace3f1e8234eb360deec3f04b18b8c26ed90b7e99eaa843b2"))

	s := enc.DecryptString(plain)

	assert.NoError(t, err, "[TestAESStringSuccess] Should not error")
	assert.Equal(t, "password-tes", s, "[TestAESStringSuccess] String should \"password-tes\"")
}
