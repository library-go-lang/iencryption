package iencryption

import (
	"encoding/base64"
)

type base64Encryption struct {
	hexKey []byte
}

func NewBase64Encryption(key string) Encryption {
	return &base64Encryption{
		hexKey: []byte(key),
	}
}

func (b base64Encryption) Encrypt(plain []byte) (ciphertext []byte, err error) {
	ciphertext = make([]byte, base64.StdEncoding.EncodedLen(len(plain)))
	base64.StdEncoding.Encode(ciphertext, plain)
	return
}

func (b base64Encryption) Decrypt(ciphertext []byte) (plainByte []byte, err error) {
	// var n int
	// plainByte = make([]byte, base64.StdEncoding.DecodedLen(len(ciphertext)))
	// n, err = base64.StdEncoding.Decode(plainByte, ciphertext)
	// plainByte = plainByte[:n]

	plainByte, err = base64.StdEncoding.DecodeString(string(ciphertext))
	return
}

func (b base64Encryption) EncryptString(ciphertext []byte) (res string) {
	return string(ciphertext)
}

func (b base64Encryption) DecryptString(ciphertext []byte) (res string) {
	return string(ciphertext)
}
