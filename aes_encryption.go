package iencryption

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io"
)

type aesEncryption struct {
	hexKey []byte
}

func NewAESEncryption(key string) Encryption {
	return &aesEncryption{
		hexKey: []byte(key),
	}
}

func (a aesEncryption) findBlock() (block cipher.Block, err error) {
	block, err = aes.NewCipher(a.hexKey)
	return
}

func (a aesEncryption) findGCM(block cipher.Block) (gcm cipher.AEAD, err error) {
	gcm, err = cipher.NewGCM(block)
	return
}

func (a aesEncryption) Encrypt(plain []byte) (encrypted []byte, err error) {
	var block cipher.Block
	var gcm cipher.AEAD

	block, err = a.findBlock()
	if err != nil {
		return
	}

	gcm, err = a.findGCM(block)
	if err != nil {
		return
	}

	//Create a nonce. Nonce should be from GCM
	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return
	}

	encrypted = gcm.Seal(nonce, nonce, plain, nil)
	return
}

func (a aesEncryption) Decrypt(src []byte) (plainByte []byte, err error) {
	var ciphertext []byte = make([]byte, hex.DecodedLen(len(src)))

	hex.Decode(ciphertext, src)

	var block cipher.Block
	var gcm cipher.AEAD
	var nonce []byte

	block, err = a.findBlock()
	if err != nil {
		return
	}

	gcm, err = a.findGCM(block)
	if err != nil {
		return
	}

	nonceSize := gcm.NonceSize()
	if len(ciphertext) < nonceSize {
		err = fmt.Errorf("missing size")
		return
	}

	nonce, ciphertext = ciphertext[:nonceSize], ciphertext[nonceSize:]

	plainByte, err = gcm.Open(nil, nonce, ciphertext, nil)
	return
}

func (a aesEncryption) EncryptString(ciphertext []byte) (res string) {
	return fmt.Sprintf("%+x", ciphertext)
}

func (a aesEncryption) DecryptString(ciphertext []byte) (res string) {
	return string(ciphertext)
}
