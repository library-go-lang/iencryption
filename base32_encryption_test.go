package iencryption_test

import (
	"gitlab.com/library-go-lang/iencryption"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBase32EncryptSuccess(t *testing.T) {
	enc := iencryption.NewBase32Encryption("test-key")

	chiper, err := enc.Encrypt([]byte("password-tes"))

	assert.NoError(t, err, "[TestBase32EncryptSuccess] Should not error")
	assert.Less(t, 0, len(chiper), "[TestBase32EncryptSuccess] byte should not empty")
	assert.Equal(
		t,
		[]byte{79, 66, 81, 88, 71, 52, 51, 88, 78, 53, 90, 71, 73, 76, 76, 85, 77, 86, 90, 81, 61, 61, 61, 61},
		chiper,
		"[TestBase32EncryptSuccess] Chipper should same",
	)
}

func TestBase32DecryptSuccess(t *testing.T) {
	enc := iencryption.NewBase32Encryption("test-key")

	plain, err := enc.Decrypt([]byte("OBQXG43XN5ZGILLUMVZQ===="))

	assert.NoError(t, err, "[TestBase32DecryptSuccess] Should not error")
	assert.Less(t, 0, len(plain), "[TestBase32DecryptSuccess] byte should not empty")
	assert.Equal(
		t,
		[]byte{112, 97, 115, 115, 119, 111, 114, 100, 45, 116, 101, 115},
		plain,
		"[TestBase32DecryptSuccess] Plain should same",
	)
}

func TestBase32DecryptFailed(t *testing.T) {
	enc := iencryption.NewBase32Encryption("test-key")

	_, err := enc.Decrypt([]byte("cGFzc3dvcmQtdGVz"))

	assert.Error(t, err, "[TestBase32DecryptFailed] Should error")
}

func TestBase32DecryptStringSuccess(t *testing.T) {
	var enc iencryption.Decryptor = iencryption.NewBase32Encryption("test-key")

	plain, err := enc.Decrypt([]byte("OBQXG43XN5ZGILLUMVZQ===="))

	s := enc.DecryptString(plain)

	assert.NoError(t, err, "[TestBase32StringSuccess] Should not error")
	assert.Equal(t, "password-tes", s, "[TestBase32StringSuccess] String should \"password-tes\"")
}

func TestBase32EncryptStringSuccess(t *testing.T) {
	var enc iencryption.Encryptor = iencryption.NewBase32Encryption("test-key")

	plain, err := enc.Encrypt([]byte("password-tes"))

	s := enc.EncryptString(plain)

	assert.NoError(t, err, "[TestBase32StringSuccess] Should not error")
	assert.Equal(t, "OBQXG43XN5ZGILLUMVZQ====", s, "[TestBase32StringSuccess] String should \"OBQXG43XN5ZGILLUMVZQ====\"")
}
